﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour {
    
    public void LoadScene() {
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    public void Menu() {
        SceneManager.LoadScene("Menu", LoadSceneMode.Single);
    }

}
