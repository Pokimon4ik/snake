﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class Test : MonoBehaviour, IPointerClickHandler
{
    private joystick.JoyStickControll JoyStickControll;

    private void Awake()
    {
        JoyStickControll = GameObject.FindGameObjectWithTag("JoyStick").GetComponent<joystick.JoyStickControll>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Vector2 jojo = Input.GetTouch(0).position;
        JoyStickControll.joyStickBG.transform.localPosition = jojo;
    }
}
