﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

namespace joystick
{
    public class JoyStickControll : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        [SerializeField]
        public Image joyStickBG;
        [SerializeField]
        private Image joyStick;
        private Vector2 inputVector; //Получение координат джостика
        private float angel;

        private void Awake()
        {
            joyStickBG = GetComponent<Image>();
            joyStick = transform.GetChild(0).GetComponent<Image>();
        }
        
        public void OnDrag(PointerEventData eventData)
        {
            angel = Vector2.SignedAngle(inputVector, Vector2.up) * -1;
            Vector2 pos;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(joyStickBG.rectTransform, eventData.position, eventData.pressEventCamera, out pos))
            {
                pos.x = (pos.x / joyStickBG.rectTransform.sizeDelta.x); //Получение координат позиции касания на джостик
                pos.y = (pos.y / joyStickBG.rectTransform.sizeDelta.y); //Получение координат позиции касания на джостик

                inputVector = new Vector2(pos.x * 2, pos.y * 2); //Установка точных координат из касания
                inputVector = (inputVector.magnitude > 1f) ? inputVector.normalized : inputVector;

                joyStick.rectTransform.anchoredPosition = new Vector2(inputVector.x * (joyStickBG.rectTransform.sizeDelta.x / 2), inputVector.y * (joyStickBG.rectTransform.sizeDelta.y / 2));
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            OnDrag(eventData);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            inputVector = Vector2.zero;
            joyStick.rectTransform.anchoredPosition = Vector2.zero; //Возврат джостика в центр
        }
        
        public float Axis()
        {
            return angel;
        }
    }
}
