﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TailMove : MonoBehaviour
{

    private GameObject snakeHead; //Сама змейка
    private GameObject target;
    private GameManager gameManager;
    private int number;

    public float Number
    {
        get { return number; }
    }

    private void Awake()
    {
        snakeHead = GameObject.FindGameObjectWithTag("MainHead");
        gameManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();
        number = gameManager.counter;
        target = gameManager.tails[number - 1];

    }

    void Start()
    {
        StartCoroutine(Move());
    }

    IEnumerator Move() //Движение хвоста змейки
    {
        while (true)
        {
            // дистанция до цели 
            var distance = (target.transform.position - transform.position).magnitude;
            var step = distance * 50 * Time.deltaTime;
            // двигаем хвост 
            var posX = target.transform.position.x;
            var posY = target.transform.position.y;
            var posZ = target.transform.position.z;
            Vector3 posTarget = target.transform.position;
            posTarget.Set(posX, posY, posZ);
            transform.position = Vector3.Lerp(transform.position, posTarget, step);
            yield return null;

        }
    }
}

