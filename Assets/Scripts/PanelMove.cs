﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PanelMove : MonoBehaviour, IDragHandler, IBeginDragHandler
{
    private GameObject snakeHead; //Сама змейка
    [SerializeField]
    private GameObject gameManagerObj;
    private GameManager gameManager;

    private void Awake()
    {
        snakeHead= GameObject.FindGameObjectWithTag("MainHead");
        gameManager = gameManagerObj.GetComponent<GameManager>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
        {
            if (eventData.delta.x > 0)
            {
                if (snakeHead.transform.rotation != Quaternion.Euler(0f, 0f, 90f))
                    snakeHead.transform.rotation = Quaternion.Euler(0f, 0f, -90f);
            }
            else if (snakeHead.transform.rotation != Quaternion.Euler(0f, 0f, -90f))
                snakeHead.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
        }
        else
        {
            if (eventData.delta.y > 0)
            {
                if (snakeHead.transform.rotation != Quaternion.Euler(0f, 0f, 180f))
                    snakeHead.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
            else if (snakeHead.transform.rotation != Quaternion.Euler(0f, 0f, 0f))
                snakeHead.transform.rotation = Quaternion.Euler(0f, 0f, 180f);

        }

    }

    public void OnDrag(PointerEventData eventData){}
}
