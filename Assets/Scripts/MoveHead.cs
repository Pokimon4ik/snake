﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using joystick;

public class MoveHead : MonoBehaviour
{
    private JoyStickControll JoyStick;
    private GameObject snakeHead; //Сама змейка
    private Vector2 snakemove;

    [SerializeField]
    [Range(0f, 10f)]
    private float speed; //Скорость змейки

    public float Speed {
        get
        {
            return speed;    
        }
        set
        {
            speed = value;
        }
    }

    void Start()
    {
        JoyStick = GameObject.FindGameObjectWithTag("JoyStick").GetComponent<JoyStickControll>();
        StartCoroutine(MoveForward()); //Движение змейки вперед
    }

    void Update()
    {
        transform.rotation = Quaternion.Euler(0f, 0f, JoyStick.Axis());
    }

    IEnumerator MoveForward() //Движение змейки вперед
    {
        while (true)
        {
            float step = speed * Time.deltaTime; //Шаг
            transform.Translate(Vector3.up * step); //Двигаем голову вперед
            yield return null;
        }
    }

}
