﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeFood : MonoBehaviour
{

    private GameObject snakeHead; //Сама змейка
    private List<GameObject> tails;
    private GameManager gameManager;
    private SpawnApple spawnApple;
    [SerializeField]
    private GameObject tailPref;
    private MoveHead moveHead;

    private void Awake()
    {
        snakeHead = GameObject.FindGameObjectWithTag("MainHead");
        gameManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();
        spawnApple = GameObject.FindGameObjectWithTag("SpawnApple").GetComponent<SpawnApple>();
        tails = gameManager.tails;
        moveHead = snakeHead.GetComponent<MoveHead>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainHead"))
        {
            spawnApple.spawn();
            gameManager.Fast();
            gameManager.counter++;
            GameObject tail = Instantiate(tailPref, tails[tails.Count - 1].transform.position - new Vector3(0, 0, 0), Quaternion.identity);
            tails.Add(tail);
            Destroy(gameObject);
        }
    }

}
