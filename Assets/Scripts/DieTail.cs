﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DieTail : MonoBehaviour {

    private GameManager gameManager;
    private MoveHead moveHead;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();
        moveHead = GameObject.FindGameObjectWithTag("MainHead").GetComponent<MoveHead>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainHead") && gameObject.GetComponent<TailMove>().Number > 2)
        {
            moveHead.Speed = 0;
            SceneManager.LoadScene("YouDie", LoadSceneMode.Single);
        }
    }

}
