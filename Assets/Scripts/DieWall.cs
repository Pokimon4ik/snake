﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DieWall : MonoBehaviour {

    private GameManager gameManager;
    private MoveHead moveHead;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<GameManager>();
        moveHead = GameObject.FindGameObjectWithTag("MainHead").GetComponent<MoveHead>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("MainHead"))
        {
            moveHead.Speed = 0;
            SceneManager.LoadScene("YouDie", LoadSceneMode.Single);
        }
    }
    
}
