﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private GameObject snakeHead; //Сама змейка
    private MoveHead moveHead;

    public List<GameObject> tails;
    public int counter = 0;

    private void Awake()
    {
        snakeHead = GameObject.FindGameObjectWithTag("MainHead");
        moveHead = snakeHead.GetComponent<MoveHead>();
        tails.Add(snakeHead);
    }

    public void Fast()
    {
        StartCoroutine(Faster());
    }

    IEnumerator Faster() //Движение змейки вперед
    {
        moveHead.Speed *= 1.5f; //Шаг + 
        yield return new WaitForSeconds(3); //Ждем 3 сек
        moveHead.Speed /= 1.5f; //Шаг - 
    }
}
