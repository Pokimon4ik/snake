﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnApple : MonoBehaviour {

    [SerializeField]
    private GameObject prefApple;

    [SerializeField]
    private GameObject canvas;
    private RectTransform rectTransform;

    [Range (1,10)]
    [SerializeField]
    private float rateSpawnApple;

    private void Awake()
    {
        rectTransform = canvas.GetComponent<RectTransform>();
    }

    void Start () {
        spawn();
	}

    //Спавнит яблоко в случайном месте
    public void spawn()
    {
        float deltaPoint = 100f;
        float pointX = Random.Range(rectTransform.rect.xMin + deltaPoint, rectTransform.rect.xMax - deltaPoint);
        float pointY = Random.Range(rectTransform.rect.yMin + deltaPoint, rectTransform.rect.yMax - deltaPoint);
        var temp = Instantiate(prefApple, new Vector3(pointX, pointY, 0), Quaternion.identity);
        temp.transform.SetParent(canvas.transform, false);
    }
}
